<?php

require_once __DIR__.'/../vendor/autoload.php'; 

use Silex\Provider\TwigServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\FormServiceProvider;
use Supexam\Providers\FormHelperServiceProvider;
use Silex\Provider\TranslationServiceProvider;

$app = new Silex\Application(); 

// Register Twig
$app->register(new TwigServiceProvider(), array(
	'twig.path'     => __DIR__.'/../views',
	'twig.options'  => array('debug' => $app['debug']),
	'twig.form.templates'   => array('form_div_layout.html.twig', 'forms/form_div_layout.twig'),
	));

$app->register(new FormServiceProvider());

$app->register( new TranslationServiceProvider() , array( 'locale' => $app['locale']  , 'locale_fallback' => 'fr' ) );
		
/*$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    $twig->addExtension(new Twig_Extensions_Extension_Text($app));

    return $twig;
    }));*/

// Register DB
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array (
	    'public' => array(
			'driver'    => 'pdo_mysql',
			'dbname'    => 'exos.cashattitude.fr',
			'host'      => "localhost" ,
			'user'      => 'root',
			'password'  => 'Qjafa1nu!',
			'charset'	=> 'utf8',
			'driverOptions' => array(1002=>'SET NAMES utf8'	)
	    ),
    ),
));

$app->get('/', function() use($app) { 
    return 'CASHATTITUDE'; 
}); 

$app->mount('/exercice', new casha\Controllers\Exercice());

$app->error(function (\Exception $e, $code) use ($app) 
{
	if ($app['debug']) {
		return $e->getMessage();
	}
	switch ($code) 
	{
		case 403 : $data['error'] = "Vous n'êtes pas autorisé à voir cette page !"; break;
		case 404 : $data['error'] = "Page inconnue..."; break;
		default: $data['error'] = $e->getMessage(); break;
	}
	return $app['twig']->render('erreur.twig' , $data);
});

return $app;
