<?php

namespace casha\Services;

use Silex\Application;

class Exercice
{
	protected static $app;
	
	public static $_canevas=null;
	
	public $id;

	public $nom;
	public $code;
	public $canevas;
	public $valeur;
	public $niveau;
	public $objectif;
	public $descriptif;
	public $note;

	public function __construct($app)
	{
		Exercice::$app = $app;
		
	}

	public function load($id)
	{
		$this->id = $id;

		$sql = "SELECT * FROM exos WHERE id = ".$this->id;
		$res = Exercice::$app['db']->fetchAssoc($sql);

		/*$this->nom = $res['nom'];
		$this->code = $res['code'];
		$this->canevas = $res['canevas'];
		$this->valeur = $res['valeur'];
		$this->niveau = $res['niveau'];
		$this->objectif = $res['objectif'];
		$this->descriptif = $res['descriptif'];
		$this->note = $res['note'];*/
		
		foreach ($res as $key => $value)
		{
		    $this->$key = $value;
		}

		return $this;
	}
}
