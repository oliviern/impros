<?php

namespace casha\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Silex\ControllerCollection;
use casha\Services;

use Symfony\Component\Validator\Constraints as Assert;

class Exercice implements ControllerProviderInterface
{
	public function connect(Application $app)
	{
		$_this = clone $this; // until php 5.4 the '$this' litteral can't be used inside a closure

		$controllers = $app['controllers_factory'];

		$controllers->get('/all', function (Application $app) use ($_this) { return $_this->all($app); });
		$controllers->post('/all', function (Application $app) use ($_this) { return $_this->getall($app); });
		
		$controllers->get('/add', function (Application $app) use ($_this) { return $_this->add($app); });
		$controllers->match('/edit/{n}', function (Application $app,$n) use ($_this) { return $_this->edit($app,$n); });
		$controllers->get('/del/{n}', function (Application $app,$n) use ($_this) { return $_this->delete($app,$n); });
		$controllers->get('/{n}', function (Application $app,$n) use ($_this) { return $_this->show($app,$n); });
		
		return $controllers;
	}

	public function all(Application $app)
	{
		$data['content'] = "TOUS LES EXOS";
		return $app['twig']->render("exercice-all.twig",$data);
	}

	public function getall(Application $app)
	{
		$sql = "SELECT * FROM exos";
		$res = $app['db']->fetchAll($sql);
		return json_encode($res);
		
		$exos = array();
		foreach ($res as $r)
			$exos[] = json_decode (json_encode ($r), FALSE);
			
		return json_encode($exos);
	}
	
	public function show(Application $app,$id)
	{
		$exercice = new Services\Exercice($app);
		$exercice->load($id);
	
		$data['content'] = $app['twig']->render("exercice.twig",["exercice"=>$exercice]);
		$data['id']=$id;
		return $app['twig']->render("exercice-show.twig",$data);
	}
	
	public function add(Application $app)
	{
		$app['db']->insert("exos", array(
			'nom' => "nouveau...",
			));

		$id = $app['db']->lastInsertId();
		
		return $app->redirect("/exercice/edit/".$id);
	}
	
	public function edit(Application $app,$id)
	{
		$exercice = new Services\Exercice($app);
		$exercice->load($id);
		
		$pform = $app['form.factory']->createBuilder('form');		
		$pform->add('nom', 'text', array( 'label' => "NOM" , 'data'=> $exercice->nom , 'required' => true));
		$pform->add('code', 'text', array( 'label' => "code" , 'data'=> $exercice->code , 'required' => true));
		$pform->add('idcanevas', 'choice',  array(
				'label' => "Canevas",
	            'choices'  => array('un', 'deux', 'trois'),
				'data' => 1,
	            'multiple' => false,
	            'expanded' => true
	        ));
		$pform->add('objectif', 'text', array( 'label' => "objectif principal" , 'data'=> $exercice->objectif , 'required' => false));
		$pform->add('descriptif', 'textarea', array( 'label'=>"Descriptif de l'exercice" , 'data'=> $exercice->descriptif , 'required' => true));
		$pform->add('note', 'textarea', array( 'label'=>"Note" , 'data'=> $exercice->note , 'required' => true));
		
		$form = $pform->getForm();
							
		$data['form'] = $form->createView();
		$data['id']=$id;
				
		if ($app['request']->isMethod('POST'))
		{			
			$f = $app['request']->request->all();
			$d = $f['form'];
			
			$app['db']->update(
				"exos",
			array (
				'nom' => $d['nom'],
				'code' => $d['code'],
				'descriptif' => $d['descriptif'],
				'objectif' => $d['objectif'],
				'note' => $d['note']
			), 
			array( 'id' => $id )
			);
			
			return $app->redirect("/exercice/".$id);
		}
		
		return $app['twig']->render("exercice-edit.twig",$data);
	}
	
	public function delete(Application $app,$id)
	{
		return "DELETE ".$id;
	}		
}
